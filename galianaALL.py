#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readGenoList.py
#  
#  Copyright 2019 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import sources.GraphConv as GRN
from sklearn.metrics import matthews_corrcoef, accuracy_score, classification_report, f1_score
import csv, os, sys, pickle, math
import numpy as np
import torch as t
import sources.utils as U
import parseAnnovarMultianno as PAM
import socket
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import KFold
from sources.ATsamples import ATsamples, IGNORE_INDEX
#if socket.gethostname() == "kusanagi":
#	device = t.device("cuda:2")
#else:
#	device = t.device("cpu")
ERASE = False
CV_SETS = 5
TOY = False
BASE = "/home/eddiewrc/galiana/"
BANNED = ["Lifetime fitness in Germany and Spain under rainfall manipulation"]
GENOLIST_PATH = BASE+"metadata/genoList.csv"
PHENOLIST_PATH = BASE+"metadata/phenoList.csv"
STUDIESLIST_PATH = BASE+"metadata/study_list.csv"
STUDIES_PATH = BASE+"arapheno/studies/"
AT_REFGENE_PATH = BASE+"atdb/AT_refGene.txt"
AT_GENESLIST = BASE+"marshalled/ATgeneslist.cPickle"
NATIONS = None#["Sweden", "Spain"]
MIN_NUM_SAMPLE_COUNTRY = 50 #CHANGE HERE THE MIN NUM OF SAMPLES FOR EACH COUNTRY
MIN_NUM_PHEN = 70
if TOY:
	AT_GENOMES_MARSHAL_PATH = BASE+"marshalled/ATgenomes.allRegions.allGenes.toy.npy" 
else:
	AT_GENOMES_MARSHAL_PATH = BASE+"marshalled/ATgenomes.allRegions.allGenes.npy" 
	AT_MARSHALLED_GENOMES_LIST = BASE+"marshalled/ATgenomes.allRegions.allGenes.list.cPickle"

selectedPhen = []
def main(args):
	if not len(args) == 3:

		print("\nUSAGE: python galiana.py cuda:2 runName\n")
		return 1
	device = args[1]
	name = args[2]
	os.system("mkdir -p " +name)
	DATA = ATsamples(GENOLIST_PATH, PHENOLIST_PATH, STUDIESLIST_PATH, STUDIES_PATH, NATIONS, MIN_NUM_SAMPLE_COUNTRY, AT_REFGENE_PATH, AT_GENOMES_MARSHAL_PATH, AT_MARSHALLED_GENOMES_LIST, AT_GENESLIST,  MIN_NUM_PHEN, BANNED, selectedPhen)
	DATA.scaleLabels(StandardScaler)
	IDS = list(DATA.genomes.keys())

	cv = 0
	totYp = []
	totY = []
	totCorresp = []
	totNation = []
	kf = KFold(CV_SETS, shuffle = False)
	for trainIds, testIds in kf.split(IDS[:]):
		print("Working on CV set %d/%d ======================" % (cv+1, CV_SETS))
		X, Y, corresp, country, gps = buildVectors(trainIds, DATA) #country contains NAMEs, they must be converted to numeric labels.
		Xscaler = U.StructuredScaler(StandardScaler())
		X = Xscaler.fit_transform(X)
		assert len(X) == len(Y)
		print("Built %d vectors of len %d" % (len(X), len(X[0])))
		model = GRN.BaselineALL(len(PAM.variantTypesHT), len(DATA.ATgenes), len(DATA.phenotypesList[:]), name= name )
		model.to(device)
		wrapper = GRN.NNwrapper(model)
		wrapper.fit(X, Y, device, epochs=70, batch_size = 8, LOG=False)
		Yp = wrapper.predict(X, device)
		print( Yp.shape)
		U.computeScoresMultiRegr(Yp, Y, IGNORE_INDEX, DATA.phenotypesList[:], DATA.phenotypes, None, None)#"train."+name+".cv"+str(cv+1))

		del X, Yp, Y, trainIds, corresp
		print( "Testing...")

		x, y, corresp, country, gps = buildVectors(testIds, DATA)
		x = Xscaler.transform(x)
		assert len(x) == len(y)
		print ("Built %d vectors of len %d" % (len(x), len(x[0])))
		yp = wrapper.predict(x, device)
		U.computeScoresMultiRegr(yp, y, IGNORE_INDEX, DATA.phenotypesList[:], DATA.phenotypes, None, None)# "test."+name+".cv"+str(cv+1))

		totYp += list(yp)
		totY += list(y)
		totCorresp += corresp
		for cor in corresp:
			totNation.append(DATA.genomes[cor]["country"])
		cv += 1
	assert len(totYp) == len(totCorresp) == len(totNation)
	print( "FINAL AVG:")
	U.computeMultiRegrPerCountry(DATA, totCorresp, totYp, totY, IGNORE_INDEX, DATA.phenotypesList[:], DATA.phenotypes, None, name+"/all."+name+".results")

	U.computeScoresMultiRegr(np.array(totYp), np.array(totY), IGNORE_INDEX, DATA.phenotypesList[:], DATA.phenotypes, None, name+"/all."+name+".resultsTotal.cv"+str(CV_SETS)+".txt")
	pickle.dump((corresp, totYp, totY, totNation), open(name+"/all."+name+".preds.cv"+str(CV_SETS)+".m","wb"))
	pickle.dump(DATA.phenotypesList, open(name+"/phenotypesList.cPickle", "wb"))
	print( "Results stored in folder %s/" % name)
	return 0

def buildVectors(ids, DATA, OLD = False): #this functions generates the feats vectors, out of the box for you
	X = []
	country = []
	gps = []
	Y = []
	corresp = []
	for i, pos in enumerate(ids):
		genomeID = list(DATA.ids)[pos]
		tmp = DATA.getGenome(genomeID)
		country.append(DATA.getCountry(genomeID))	
		gps.append(DATA.genomes[genomeID]["gps"])
		X.append(DATA.getGenome(genomeID))
		Y.append(DATA.phenotypesAsFeatures(genomeID)[:])
		corresp.append(genomeID)
	return np.array(X, dtype=np.float32), np.array(Y), corresp, country, gps


if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
