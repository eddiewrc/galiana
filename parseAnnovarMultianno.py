#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  runAnnovar.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@mira>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import sources.parseMetaData as PMD
import sys, os, pickle, marshal
import numpy as np
import torch as t
import galianaALL as GA
NP_DATATYPE_VARIANTS = np.uint16 #np.int16
#Chr	Start	End	Ref	Alt	Func.refGene	Gene.refGene	GeneDetail.refGene	ExonicFunc.refGene	AAChange.refGene	Otherinfo

#variantTypesHT = {"exonic_nonsynonymous":"eSNV", "exonic_nonframeshift_insertion":"eNFI", "exonic_nonframeshift_deletion":"eNFD", "exonic_stoploss":"eSL", "exonic_frameshift_insertion":"eFI", "exonic_frameshift_deletion":"eFD", "UTR3":"UTR3", "UTR5":"UTR5", "ncRNA_exonic":"eNcRNA", "ncRNA_intronic":"intrNcRNA", "upstream":"UP", "downstream":"DWN", "intergenic":"inter", "intronic":"intr", "splicing":"spl", "ncRNA_splicing":"splNcRNA", "exonic_stopgain":"eSG"}
variantTypesHT =  {"exonic_nonsynonymous":0, "exonic_nonframeshift_insertion":1, "exonic_nonframeshift_deletion":2, "exonic_stoploss":3, "exonic_frameshift_insertion":4, "exonic_frameshift_deletion":5, "UTR3":6, "UTR5":7, "ncRNA_exonic":8, "ncRNA_intronic":9, "upstream":10, "downstream":11, "intergenic":12, "intronic":13, "splicing":14, "ncRNA_splicing":15, "exonic_stopgain":16}

def readATgenes(f):
	"""
	This function reads all the AT genes used by annovar to annotate AT WGS. It is used also to create the numpy matrix containing the AT VCF data.

	Returns {ATgeneName:unique_number}
	"""
	ifp = open(f)
	line = ifp.readline()
	genes = {}
	c = 0
	while len(line) > 0:
		tmp = line.strip().split("\t")[12].split(":")[1]
		#print tmp
		if not genes.has_key(tmp):
			genes[tmp] = c
			c+=1
		line = ifp.readline()
	print( "Found %d genes for AT" % len(genes))
	return genes

def parseAnnovarMultiannoPerGeneLookup(f, ATgeneList, missenseOnly = True, onlyRegions=None, onlyGenes = None):
	"""
	To save space, this function reads the AT VCF and stores the variants PER GENE. 
	There are len(variantTypesHT) types of variants that can occur on each gene.
	The counts of the variants are stored into a numpy matrix.

	Returns np.array([[variants gene1], [variants gene2], ...]) 
	"""
	ifp = open(f)
	line = ifp.readline()
	assert "Chr" in line and "Start" in line
	line = ifp.readline()
	numMuts = 0
	db = np.zeros((len(ATgeneList), len(variantTypesHT)), dtype=NP_DATATYPE_VARIANTS)#prova unsigned int8
	if onlyGenes != None:
		onlyGenes = set(onlyGenes)
	while len(line) > 0:
		tmp = line.split("\t")
		crom = tmp[0]
		pos = (int(tmp[1]), int(tmp[2]))
		mut = (tmp[3], tmp[4])
		region = tmp[5].split(";")[0]
		
		if (onlyRegions != None and len(onlyRegions) > 0) and not region in onlyRegions:
			line = ifp.readline()
			continue	
				
		gene = parseGene(tmp[6])
		#print gene
		#raw_input()
		if onlyGenes != None and not gene in onlyGenes:
			line = ifp.readline()
			continue	

		vartype = tmp[8].split(";")[0]
		if "unknown" in vartype:
			line = ifp.readline()
			continue
		if " SNV" in vartype:
			vartype = vartype.split(" ")[0]
		else:
			vartype = vartype.replace(" ", "_")
		if "exonic" in region and missenseOnly and  "synonymous" == vartype:
			line = ifp.readline()
			continue	
	
		if not ATgeneList.has_key(gene):
			raise Exception("ERROR: gene %s not in AT list"%gene)

		if region == "exonic":
			db[ATgeneList[gene]][variantTypesHT[region+"_"+vartype]] += 1
		else:
			db[ATgeneList[gene]][variantTypesHT[region]] += 1
		numMuts += 1
		line = ifp.readline()
	print( "Max num of muts per gene: ", np.max(db))
	print ("Found %d genes and %d mutations" % (len(db), numMuts))
	return db	

def parseGene(s):
	tmp = s.strip().split(";")
	for i in tmp:
		if "gene:" in i:
			return i.split(":")[1]
	raise Exception("ERROR: no gene found in ", s)
		
def main():
	FAST = False
	ATgenes = readATgenes("atdb/AT_refGene.txt")
	genomes = PMD.readGenoList("metadata/genoList.csv")
	db = []
	genomesList = {}
	if not FAST:
		ids = genomes.keys()
	else:
		ids = ["7213", "7217", "7218"]
	i = 0
	for g in ids:
		if genomes[g]["nPhen"] == 0:
			continue
		print ("Working on %s ( %d )" % ( g, i ))
	#for f in os.listdir("annovarGenomes"):
	#	if not "_multianno.txt" in f:
	#		continue
		genomesList[g] = i
		db.append(parseAnnovarMultiannoPerGeneLookup("annovarGenomes/intersection_"+g+".AT_multianno.txt", ATgenes, onlyRegions=None, onlyGenes = None))
		i += 1
	np.save(GA.AT_GENOMES_MARSHAL_PATH, np.array(db))#, open("ATgenomes.allRegions.allGenes.m","w"))
	cPickle.dump(genomesList, open(GA.AT_MARSHALLED_GENOMES_LIST, "w"))
	cPickle.dump(ATgenes, open(GA.AT_GENESLIST, "w"))
	return 0
	
if __name__ == '__main__':
	main()
