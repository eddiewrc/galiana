
import parseAnnovarMultianno as PAM
import sources.parseMetaData as PMD
import sources.joinAnnotations as JA
import os, pickle
import numpy as np
ERASE = False
IGNORE_INDEX = -9999

class ATsamples(object):

	"""
	Class that is supposed to manage ALL the data issues in an incapsulated way.
	"""

	def __init__(self, GENOLIST_PATH, PHENOLIST_PATH, STUDIESLIST_PATH, STUDIES_PATH, NATIONS_LIST, MIN_NUM_SAMPLE_COUNTRY, AT_REFGENE_PATH, AT_GENOMES_MARSHAL_PATH, AT_MARSHALLED_GENOMES_LIST, AT_GENESLIST, MIN_NUM_PHEN, bannedStudies = [], selectedPhen = [], bannedPhen = []):
		"""
		Reads info about the genomes (name, country, collector, numPhen), the studies (name, desc, numPhen), the phenotypes (phenID, descr, growth cond.). 
		Reads the genome annotation (the phenotype values for each genome ID).
		Filters out the countries with less samples than MIN_NUM_SAMPLE_COUNTRY, and the corresponding genomes.
		
		"""
		genomes = PMD.readGenoList(GENOLIST_PATH) #{genomeID: {"name":name,"country":country, "collector":collector, "nPhen":phen, "gps":gps_tuple}}
		if NATIONS_LIST == None or len(NATIONS_LIST) == 0:
			self.nations = self.getNumSamplesPerCountry(genomes, MIN_NUM_SAMPLE_COUNTRY)
		else:
			self.nations = {}
			for n in NATIONS_LIST:
				self.nations[n] = -1
		print( "## Initial %d genomes" % len(genomes))
		tmpDB = {}
		for g in genomes.items():
			if g[1]["nPhen"] == 0 or g[1]["country"] not in self.nations.keys():
				pass#del genomes[g[0]]
			else:
				tmpDB[g[0]] = g[1]
		self.genomes = tmpDB #thanks python3
		del genomes
		print( "## After filtering: %d genomes" % len(self.genomes))
		self.studies = PMD.readStudyList(STUDIESLIST_PATH, bannedStudies) # {studyID:{"name":row["name"], "desc":row["description"], "nPhen":row["phenotype_count"]}
		self.phenotypes = PMD.readPhenoList(PHENOLIST_PATH, bannedStudies, selectedPhen, bannedPhen) #{phenotypeName:{"pid":row["phenotype_id"], "study":row["study"], "desc":row["scoring"], "growth":row["growth_conditions"]}}
		self.genomeAnnotation, self.phenotypes = JA.integrateAnnotations(self.studies, self.phenotypes, self.genomes, STUDIES_PATH, MIN_NUM_PHEN) #{genomeID:{phenotypeName:[list of values]}}
		########## filter out countries with few samples
		for g in self.genomes.items():
			if not g[0] in self.genomeAnnotation:
				del self.genomes[g[0]]
		self.ids = self.genomes.keys()
		print ("Working on %d genomes" % len(self.genomes))
		if not os.path.exists(AT_GENOMES_MARSHAL_PATH) or ERASE:
			i = 0
			genomesList = {}
			db = []
			ATgenes = PAM.readATgenes("atdb/AT_refGene.txt")
			for g in self.genomes.keys():
				genomesList[g] = i
				db.append(PAM.parseAnnovarMultiannoPerGeneLookup("/home/eddiewrc/galiana/annovarGenomes/intersection_"+g+".AT_multianno.txt", ATgenes, onlyRegions=None, onlyGenes = None))
				i += 1
			np.save(AT_GENOMES_MARSHAL_PATH, np.array(db))#, open("ATgenomes.allRegions.allGenes.m","w"))
			pickle.dump(genomesList, open(AT_MARSHALLED_GENOMES_LIST, "wb"))
			pickle.dump(ATgenes, open(AT_GENESLIST, "wb"))

		print ("Reading data from ",AT_GENOMES_MARSHAL_PATH )
		self.genomeDB = np.load(AT_GENOMES_MARSHAL_PATH)
		self.genomesLookup = pickle.load(open(AT_MARSHALLED_GENOMES_LIST, "rb"))
		self.ATgenes = pickle.load(open(AT_GENESLIST, "rb"))
		#########################HERE THE MAGIC STARTS###################################
		self.phenotypesList = list(self.phenotypes.keys())
		pickle.dump(self.phenotypesList, open("/home/eddiewrc/galiana/marshalled/phenotyphesList.cPickle", "wb"))

	def getNumSamplesPerCountry(self, genomes, THR):
		"""
		Filters out the countries with less than THR samples.
		"""
		nations = {}
		for g in genomes.items():
			if not g[1]["country"] in nations:
				nations[g[1]["country"]] = 0
			nations[g[1]["country"]] += 1
		tmpDB = {}
		for n in nations.items():
			if n[1] < THR:
				#print "Remove %s (%d samples)" % (n[0], n[1])
				#del nations[n[0]]
				pass
			else:
				tmpDB[n[0]] = n[1]
		nations = tmpDB
		print ("Remaining %d countries" % len(nations))
		print (sorted(nations.items(), key=lambda x:x[1]))
		return nations # {country:numSample}

	def countryBasedSplitSklearn(self):
		"""
		Returns the country-based clusters for KFold sklearn.
		"""
		clusters = []
		countries = {}
		countriesReverse = {}
		i = 0
		
		for g in self.genomes.items():
			co = g[1]["country"]
			if co in self.nations.keys():
				if not co in countries:
					countries[co] = i
					countriesReverse[i] = co
					i+=1
			else:
				raise Exception("this shoulndt be there")
		assert max(countries.values())+1 == len(countries)
		ids = []
		for i, g in enumerate(self.genomes.keys()):
			clusters.append(countries[self.genomes[g]["country"]])
			ids.append(g)
		assert ids == self.ids
		return clusters, ids, countriesReverse

	def getCountry(self, gid):
		"""
		Given a genome ID, returns the country
		"""
		assert gid in self.ids
		return self.genomes[gid]["country"]

	def getGenome(self, gid):
		"""
		Given a genome id, returns the genetic data
		"""
		assert gid in self.ids
		return self.genomeDB[self.genomesLookup[gid]]

	def phenotypesAsFeatures(self, gid):
		"""
		Given a genome ID, returns the phenotype values.
		"""
		#print phenotypes
		y = np.ones(len(self.phenotypesList), dtype=np.float16) * IGNORE_INDEX
		for p in self.genomeAnnotation[gid].items():
			#print p
			#raw_input()
			y[self.phenotypesList.index(p[0])] = np.mean(p[1])
		return y

	def scaleLabels(self, scaler):
		LS = LabelScaler(scaler)
		self.genomeAnnotation = LS.fit_transform(self.phenotypesList, self.genomeAnnotation)



class LabelScaler:

	def __init__(self, scaler):
		self.scalerTemplate = scaler
		self.scalers = []

	def fit_transform(self, phenotypesList, genomeAnnotation):
		Y = []
		newAnnotations = {}
		for p in phenotypesList:
			#print p
			y = []
			corresp = []
			#assert genomeAnnotation.items() == genomeAnnotation.items()
			for i, annot in enumerate(genomeAnnotation.items()):
				#print annot
				#raw_input()
				if not p in annot[1]:
					continue
				val = np.mean(annot[1][p])
				corresp.append(annot[0])
				y.append(val)
			#print corresp
			#print y
			scaler = self.scalerTemplate()
			y1 = scaler.fit_transform(np.array(y).reshape(-1,1))
			y1 = list(y1)
			self.scalers.append(scaler)
			#print self.scalers[0].mean_, self.scalers[0].var_#print y1
			i = 0
			for _, annot in enumerate(genomeAnnotation.items()):
				if not annot[0] in newAnnotations:
					newAnnotations[annot[0]] = {}
				if not p in annot[1]:
					continue
				#print corresp[i], annot[0]
				assert corresp[i] == annot[0]				
				newAnnotations[annot[0]][p] = y1.pop(0)
				i+=1
			assert len(y1) == 0
		return newAnnotations		


