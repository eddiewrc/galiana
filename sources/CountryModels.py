#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pred1.py
#  
#  Copyright 2018 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from sources.Logger import MetaLogger
import matplotlib.pyplot as plt
import os, copy, random, time, math, socket
from sys import stdout
import numpy as np
import torch as t
from torch.autograd import Variable
from torch.nn.utils.rnn import PackedSequence, pack_padded_sequence, pad_packed_sequence
from torch.utils.data import Dataset, DataLoader
from scipy.stats import pearsonr
import Losses as L
from ResNets import LeakyHardTanh, ResidualBlock
#t.manual_seed(0)


class GraphConvolution(t.nn.Module):
	
	def __init__(self, in_features, out_features, adjMatrix, bias=True):
		super(GraphConvolution, self).__init__()
		self.adj = adjMatrix
		self.in_features = in_features
		self.out_features = out_features
		self.weight = t.nn.Parameter(t.FloatTensor(in_features, out_features))
		if bias:
			self.bias = t.nn.Parameter(t.FloatTensor(out_features))
		else:
			self.register_parameter('bias', None)
		#self.reset_parameters()

	def reset_parameters(self):
		stdv = 1. / math.sqrt(self.weight.size(1))
		self.weight.data.uniform_(-stdv, stdv)
		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, input):
		batch_size = input.size()[0]
		#print input.size()
		support = t.matmul(input.squeeze(), self.weight)
		#print support.size()
		output = t.stack([t.sparse.mm(self.adj, support[i,:].float()) for i in range(batch_size)])
		#print output.size()
		#output = t.mm(self.adj, support)
		if self.bias is not None:
			return output + self.bias
		else:
			return output

class Baseline1Specific(t.nn.Module):

	def __init__(self, genesize, numGenes, numOut, name = "NN"):
		super(Baseline1Specific, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		
		self.geneInputSize = genesize
		self.numGenes = numGenes
		
		self.nn = t.nn.Sequential( t.nn.Linear(self.geneInputSize, 10), LeakyHardTanh(), t.nn.Linear(10,1), LeakyHardTanh())
		#self.final = t.nn.Sequential( t.nn.Linear(self.numGenes, 20), LeakyHardTanh())
		self.moduleList = t.nn.ModuleList([ t.nn.Sequential(t.nn.Dropout(0.1), t.nn.Linear(self.numGenes, 10), LeakyHardTanh(), t.nn.Linear(10,1)) for i in range(numOut)])
		#self.final = t.nn.Sequential(t.nn.Dropout(0.1), t.nn.Linear(self.numGenes, 10), t.nn.LeakyReLU(), t.nn.Linear(10,1))
		#self.apply(self.init_weights)
		
	def forward(self, x):	
		batch_size = x.size()[0]
		o = self.nn(x).view(batch_size, -1)
		#print o.size()
		#o = self.final(o)
		outList = []
		for m in self.moduleList:
			outList.append(m(o))
		outList = t.cat(outList,1)
		return outList

	def init_weights(self, m):
		if isinstance(m, t.nn.Conv1d) or isinstance(m, t.nn.Linear) or isinstance(m, t.nn.Bilinear) or isinstance(m, GraphConvolution):
			print "Initializing weights...", m.__class__.__name__
			#t.nn.init.normal(m.weight, 0, 0.01)
			t.nn.init.xavier_uniform(m.weight)
			m.bias.data.fill_(0.1)
		elif isinstance(m, t.nn.Embedding):
			print "Initializing weights...", m.__class__.__name__
			t.nn.init.xavier_uniform(m.weight)	

class Baseline1NN(t.nn.Module):
	def __init__(self, genesize, numGenes, numOut, name = "NN"):
		super(Baseline1NN, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		
		self.geneInputSize = genesize
		self.numGenes = numGenes
		
		self.nn = t.nn.Sequential( t.nn.Linear(self.geneInputSize, 10), LeakyHardTanh(), t.nn.Linear(10, 1), LeakyHardTanh())
		self.final = t.nn.Sequential(t.nn.Dropout(0.25), t.nn.Linear(self.numGenes, 100), t.nn.Dropout(0.2), LeakyHardTanh(), t.nn.Linear(100, numOut))
		#self.final = t.nn.Sequential(t.nn.Dropout(0.1), t.nn.Linear(self.numGenes, 10), t.nn.LeakyReLU(), t.nn.Linear(10,1))
		self.apply(self.init_weights)
		
	def forward(self, x):	
		batch_size = x.size()[0]
		o = self.nn(x).view(batch_size, -1)
		#print o.size()
		o = self.final(o.squeeze())
		#o = (t.sum(o.squeeze(), 1)/o.size(1)).unsqueeze(1)		
		return o
	
	def init_weights(self, m):
		if isinstance(m, t.nn.Conv1d) or isinstance(m, t.nn.Linear) or isinstance(m, t.nn.Bilinear) or isinstance(m, GraphConvolution):
			print "Initializing weights...", m.__class__.__name__
			#t.nn.init.normal(m.weight, 0, 0.01)
			t.nn.init.xavier_uniform(m.weight)
			m.bias.data.fill_(0.1)
		elif isinstance(m, t.nn.Embedding):
			print "Initializing weights...", m.__class__.__name__
			t.nn.init.xavier_uniform(m.weight)	

class GpsNN(t.nn.Module):
	def __init__(self, numOut, name = "NN"):
		super(GpsNN, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		
		
		self.nn = t.nn.Sequential( t.nn.Linear(2, 100), t.nn.LayerNorm(100), t.nn.LeakyReLU())
		self.final = t.nn.Sequential( t.nn.Linear(100, numOut))
		#self.final = t.nn.Sequential(t.nn.Dropout(0.1), t.nn.Linear(self.numGenes, 10), t.nn.LeakyReLU(), t.nn.Linear(10,1))
		self.apply(self.init_weights)
		
	def forward(self, x, GET_ACT = False):	
		#print x.size()	
		o1 = self.nn(x)
		#print o.size()
		o = self.final(o1.squeeze())
		#o = (t.sum(o.squeeze(), 1)/o.size(1)).unsqueeze(1)		
		if GET_ACT:
			return o1, o
		else:
			return o
	
	def init_weights(self, m):
		if isinstance(m, t.nn.Conv1d) or isinstance(m, t.nn.Linear) or isinstance(m, t.nn.Bilinear) or isinstance(m, GraphConvolution):
			print "Initializing weights...", m.__class__.__name__
			t.nn.init.normal(m.weight, 0, 0.01)
			#t.nn.init.xavier_uniform(m.weight)
			m.bias.data.fill_(1)
		elif isinstance(m, t.nn.Embedding):
			print "Initializing weights...", m.__class__.__name__
			t.nn.init.xavier_uniform(m.weight)		

class BaselineNN(t.nn.Module):
	def __init__(self, genesize, numGenes, numOut, name = "NN"):
		super(BaselineNN, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		
		self.geneInputSize = genesize
		self.numGenes = numGenes
		
		self.nn = t.nn.Sequential( t.nn.Linear(self.geneInputSize, 1), t.nn.LeakyReLU())
		self.final = t.nn.Sequential(t.nn.Linear(self.numGenes, 100), t.nn.LeakyReLU(), t.nn.Linear(100, numOut))
		#self.final = t.nn.Sequential(t.nn.Dropout(0.1), t.nn.Linear(self.numGenes, 10), t.nn.LeakyReLU(), t.nn.Linear(10,1))
		self.apply(self.init_weights)
		
	def forward(self, x, GET_ACT = False):	
		#print x.size()	
		o1 = self.nn(x)
		#print o.size()
		o = self.final(o1.squeeze())
		#o = (t.sum(o.squeeze(), 1)/o.size(1)).unsqueeze(1)		
		if GET_ACT:
			return o1, o
		else:
			return o
	
	def init_weights(self, m):
		if isinstance(m, t.nn.Conv1d) or isinstance(m, t.nn.Linear) or isinstance(m, t.nn.Bilinear) or isinstance(m, GraphConvolution):
			print "Initializing weights...", m.__class__.__name__
			t.nn.init.normal(m.weight, 0, 0.01)
			#t.nn.init.xavier_uniform(m.weight)
			m.bias.data.fill_(1)
		elif isinstance(m, t.nn.Embedding):
			print "Initializing weights...", m.__class__.__name__
			t.nn.init.xavier_uniform(m.weight)		
			
class GraphConvModel(t.nn.Module):
	
	def __init__(self, genesize, numGenes, numOut, adj, device, name = "NN"):
		super(GraphConvModel, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		print "Building adj..."
		self.adj = self.createSymmNormAdjSparse(adj).to(device)
		print self.adj.size()
		self.geneInputSize = genesize
		self.numGenes = numGenes
		
		self.gcn = t.nn.Sequential(GraphConvolution(self.geneInputSize, 10, self.adj), t.nn.Dropout(0.2), t.nn.Tanh(), GraphConvolution(10, 5, self.adj), t.nn.Dropout(0.3), t.nn.Tanh(), GraphConvolution(5, 1, self.adj), t.nn.Tanh(), t.nn.Dropout(0.4))
		self.final = t.nn.Sequential(t.nn.Linear(self.numGenes, numOut))
		self.apply(self.init_weights)

	
	def init_weights(self, m):
		if isinstance(m, t.nn.Conv1d) or isinstance(m, t.nn.Linear) or isinstance(m, t.nn.Bilinear) or isinstance(m, GraphConvolution):
			print "Initializing weights...", m.__class__.__name__
			t.nn.init.xavier_uniform(m.weight)
			m.bias.data.fill_(0.01)
		elif isinstance(m, t.nn.Embedding):
			print "Initializing weights...", m.__class__.__name__
			t.nn.init.xavier_uniform(m.weight)
			
	def forward(self, x):	
		o = self.gcn(x)
		o = self.final(o.squeeze())
		return o

	def createSymmNormAdjSparse(self, adj):
		# add self interactions
		selfInt = self.sparseEye(adj.size()[0])
		adj = adj.add(selfInt)
		del selfInt
		d = self.buildDegreeMatrixSparse(adj)
		tmp = d.mul(adj).mul(d)
		#tmp = t.sparse.mm(t.sparse.mm(d, adj), d)
		return tmp

	def sparseEye(self, diag):
		i = 0
		coord = [[],[]]
		if type(diag) == list:
			values = diag
			size = len(diag)
		elif type(diag) == int:
			values = []
			size = diag
		else:
			raise Exception("Unexpected diag: ", diag)

		while i < size:
			coord[0].append(i)
			coord[1].append(i)
			if type(diag) == int:
				values.append(1)
			i+=1
		seye = t.sparse.FloatTensor(t.LongTensor(coord), t.FloatTensor(values), t.Size([size, size]))
		return seye


	def buildDegreeMatrixSparse(self, adj):
		tmp = t.sparse.sum(adj,0)
		#tmp = t.pow(tmp, -0.5)
		tmp = tmp.pow(-0.5)
		#raw_input()
		tmp = self.sparseEye(tmp.to_dense().tolist())
		#print tmp.size()
		#raw_input()
		return tmp


class ATDataset(Dataset):
    
	def __init__(self, X, Y):	
	
		assert len(Y) == len(X)		
		self.X = t.tensor(X, dtype=t.float32)
		self.Y = t.tensor(Y, dtype=t.long)
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx], self.Y[idx]

class ATpredDataset(Dataset):
    
	def __init__(self, X):	
		self.X = t.tensor(X, dtype=t.float32)
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx]

class NNwrapper():

	def __init__(self, model):
		if type(model) == str:
			self.model = t.load(model)
		else:
			self.model = model

	def fit(self, originalX, originalY, device, epochs = 50, batch_size=20, save_model_every=10, weight_decay = 1e-5, learning_rate = 1e-2, silent = False, IGNORE_INDEX = -9999, LOG=False):
		########DATASET###########
		t1 = time.time()
		dataset = ATDataset(originalX, originalY)
		t2 = time.time()
		print "Dataset created in %.2fs" % (t2 - t1)
		#######MODEL##############		
		if LOG:
			self.logger = MetaLogger(self.model, port = 6001)
		self.model.train()	
		print "Start training"
		########LOSS FUNCTION######
		lossfn = t.nn.CrossEntropyLoss()
		#lossfn = L.LossWrapper(t.nn.SmoothL1Loss(), ignore_index=IGNORE_INDEX)
		########OPTIMIZER##########	
		parameters =list(self.model.parameters())
		p = []
		for i in parameters:
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)
		del p        
		self.model.train()
		print "Training mode: ", self.model.training

		#parameters = list(self.model.g.parameters())+list(self.model.final2.parameters())+list(self.model.pathLayer.parameters())
		optimizer = t.optim.Adam(parameters, lr=learning_rate, weight_decay=weight_decay)
		scheduler = t.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.3, patience=3, verbose=True, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08)
		
		########DATALOADER#########
		t1 = time.time()
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=True, sampler=None, num_workers=0)
		t2 = time.time()
		print "Dataloader created in %.2fs" % (t2 - t1)

		print "Start epochs"
		e = 1
		minLoss = 1000000000
		while e < epochs:			
			errTot = 0
			i = 1
			start = time.time()
			for sample in loader:
				optimizer.zero_grad()
				x, y = sample
				x = x.to(device)
				y = y.to(device)
				#print y
				yp = self.model.forward(x)
				loss = lossfn(yp, y)
				loss.backward()	
				optimizer.step()
				errTot += loss.data
				i+=batch_size						
				perc = (100*i/float(len(dataset))	)	
				#stdout.write("\nepoch=%d %d (%3.2f%%), errBatch=%f" % (e, i, perc, loss.data[0]))
				#stdout.flush()				
			end = time.time()	
			if LOG:
				self.logger.writeTensorboardLog(e, errTot, [], [], end-start)
			if not silent:				
				print " epoch %d, ERRORTOT: %f (%fs)" % (e, errTot, end-start)
			scheduler.step(errTot)
			if e % save_model_every == 0:
				print "Store model ", e
				#t.save(self.model, "models/"+self.model.name+".iter_"+str(e)+".t")							
			stdout.flush()	
			e += 1	
		if LOG:
			self.logger.shutdown()
		#t.save(self.model, "models/"+self.model.name+".final.t")
	
	def predict(self, X, device):
		self.model.eval()
		print "Predicting..."
		softmax = t.nn.Softmax()
		dataset = ATpredDataset(X)
		loader = DataLoader(dataset, batch_size=len(X), shuffle=False, sampler=None, num_workers=0)
		preds1 = []
		act = []
		for sample in loader:
			x = sample
			x = x.to(device)
			y_pred = softmax(self.model.forward(x))
			preds1 += y_pred.data.squeeze().tolist()			
		return np.array(preds1)
		
		

if __name__ == '__main__':
	from iongreen2 import main
	main()
	#mainEVAL()
