import torch as t
from torch.autograd import Variable

class LossWrapper():

	"""
	Class that wraps pytorch LogitBCE allowing for ignore index.
	"""

	def __init__(self, loss, ignore_index = -1):
		self.loss = loss
		self.ignore_index = ignore_index
		print (" >>> IGNORE INDEX:" , self.ignore_index)
	
	def __call__(self, input, target):
		input = input.view(-1)
		target = target.view(-1)
		mask = target.ne(self.ignore_index)
		input = t.masked_select(input, mask)
		target = t.masked_select(target, mask)
		return self.loss(input, target)

class WeightedMSELoss():

	def __init__(self, ignore_index = -1):
		self.ignore_index = ignore_index
		print (" >>> IGNORE INDEX:" , self.ignore_index)
		return
		
	def __call__(self, input, target, weights):
		tmp = t.ones(input.size()).to(input.device)
		tmp = t.mul(tmp, weights)
		tmp = tmp.view(-1)
		input = input.view(-1)
		target = target.view(-1)
		mask = target.ne(self.ignore_index)
		input = t.masked_select(input, mask)
		target = t.masked_select(target, mask)
		tmp = t.masked_select(tmp, mask)
		target = (input-target).pow(2)
		target = t.mul(target, tmp).mean()
		return target

	def ciao1(self, input, target, weights):
		assert input.size() == target.size()
		target = (input - target)**2
		target = t.mean(target, 0).unsqueeze(0) / input.size()[0]
		#print target.size(), weights.size()
		target = t.matmul(target, weights.unsqueeze(1)).squeeze()
		#print target.size()	
		return target
		
class PearsonLoss(t.nn.Module):

	def __init__(self, ignore_index = -1, negative=False):
		self.ignore_index = ignore_index
		self.type = "regression"
		self.negative = negative

	def __call__(self, input, target):
		return self.forward(input, target)

	def forward(self, input, target):
		#print "qui", input.size(), target.size()
		input = input.view(-1)
		target = target.view(-1)
		mask = target.ne(self.ignore_index)
		input = t.masked_select(input, mask)
		target = t.masked_select(target, mask)
		#print "qu", input.size(), target.size()
		vx = input - t.mean(input)
		vy = target - t.mean(target)
		#print vx, vy
		cost = t.sum(vx * vy) / (t.sqrt(t.sum(vx ** 2)) * t.sqrt(t.sum(vy ** 2)))
		if self.negative:
			cost = -1 * cost
		return cost

