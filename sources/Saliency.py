from GraphConv import ATDataset, NNwrapper
import Losses as L
import torch as t
from torch.utils.data import Dataset, DataLoader
import numpy as np
import sys

class AddGaussianNoise(object):
	def __init__(self, mean=0., std=1.):
		self.std = std
		self.mean = mean

	def __call__(self, tensor):
		return tensor + t.randn(tensor.size()) * self.std + self.mean

	def __repr__(self):
		return self.__class__.__name__ + '(mean={0}, std={1})'.format(self.mean, self.std)


class IntegratedSaliencyWrapper(NNwrapper, object):
	"""
	Class that implements the standard gradient based saliency maps.
	In the case of Galiana, stores gradients both at the "gene" neuron level and aggregates the features. Use the perSample method. Not sure what the other does.
	To save space, gives only the best 100 genes (instead of N_PHEN * 27k)
	"""

	def __init__(self, model, n_iter = 800):
		super(IntegratedSaliencyWrapper, self).__init__(model)
		self.n_iter = n_iter
	
	def predictSALperSample(self, X, Y, corresp, device, saliency, genes, phenList, IGNORE_INDEX = -9999):

		#print len(phenList), Y.shape
		assert len(phenList) == Y.shape[1]
		saliencyG = saliency[0]
		saliencyX = saliency[1]
		self.model.eval()
		print "Predicting SAL..."
		dataset = ATDataset(X, Y)
		for phen, pName in enumerate(phenList):
			loader = DataLoader(dataset, batch_size=1, shuffle=False, sampler=None, num_workers=0)
			tmpGradsX = np.zeros((self.model.numGenes, self.model.geneInputSize))
			assert len(X) == len(Y) == len(corresp)
			i = 0
			for sample in loader:#runs only once
				x1, y1 = sample
				assert y1.size()[0] == 1
				if y1[0,phen] == IGNORE_INDEX:
					#print "skip ",phen
					continue

				sys.stdout.write("\rPhen %d/%d, Sample %d/%d" % (phen, len(phenList), i+1, len(X)))
				if not saliencyX.has_key(corresp[i]):
					saliencyX[corresp[i]] = {}

				scores = [0,0]
				j = 0
				while j < self.n_iter:
					#print j
					x = x1.detach().clone()
					baseline = t.zeros((self.model.numGenes, self.model.geneInputSize)).to(device)
					x = x.to(device)
					x = (float(j)/float(self.n_iter)) * x
					#print x.size()
					x.requires_grad_()
					parameters = list(self.model.parameters())+ [x]
					optimizer = t.optim.Adam(parameters)#+[{"params":[x], "lr":1}])
					optimizer.zero_grad()
					#y = y.to(device)
					yp = self.model.forward(x)
					assert yp.size()[0] == 1
					if j == 0:
						scores[0] = float(yp[0, phen].cpu())
					if j == self.n_iter-1:
						print yp[0, phen]
						scores[1] = float(yp[0, phen].cpu())
					yp[0,phen].backward(retain_graph=True)
					#print tmpGradsG.shape, self.model.middle[1].weight.grad.cpu().size()
					#print tmpGradsX.shape, x.grad[0,:,:].cpu().size()
					tmpGradsX += np.array(x.grad[0,:,:].cpu())
					optimizer.zero_grad()
					j+=1

				tmpGradsX = (tmpGradsX / float(self.n_iter)) * np.array(x1.squeeze().cpu())
				print np.sum(tmpGradsX), scores[1]-scores[0]
				assert np.abs(np.sum(tmpGradsX) - (scores[1]-scores[0])) < 0.1 * np.abs(scores[1]-scores[0]) 
				print tmpGradsX.shape, np.sum(tmpGradsX[:,:], 1).shape
				tmp = np.abs(np.sum(tmpGradsX[:,:], 1))
				print tmp.shape
				saliencyX[corresp[i]][phen] = sortGenes(tmp, genes, 100)
				assert t.sum(self.model.middle[1].weight.grad) == 0
				assert t.sum(x.grad) == 0
				i += 1
		return saliencyX



class SmoothSaliencyWrapper(NNwrapper, object):
	"""
	Class that implements the standard gradient based saliency maps.
	In the case of Galiana, stores gradients both at the "gene" neuron level and aggregates the features. Use the perSample method. Not sure what the other does.
	To save space, gives only the best 100 genes (instead of N_PHEN * 27k)
	"""

	def __init__(self, model, noisePerc = 0.1, n_iter = 50):
		super(SmoothSaliencyWrapper, self).__init__(model)
		self.noisePerc = noisePerc
		self.n_iter = n_iter
	
	def predictSALperSample(self, X, Y, corresp, device, saliency, genes, phenList, IGNORE_INDEX = -9999):

		#print len(phenList), Y.shape
		assert len(phenList) == Y.shape[1]
		saliencyG = saliency[0]
		saliencyX = saliency[1]
		self.model.eval()
		print "Predicting SAL..."
		dataset = ATDataset(X, Y)
		for phen, pName in enumerate(phenList):
			loader = DataLoader(dataset, batch_size=1, shuffle=False, sampler=None, num_workers=0)
			tmpGradsX = np.zeros((self.model.numGenes, self.model.geneInputSize))
			tmpGradsG = np.zeros((50, self.model.numGenes))
			assert len(X) == len(Y) == len(corresp)
			i = 0
			print "Doing SmoothGrad with sigma=", self.n_iter 
			for sample in loader:#runs only once
				x1, y1 = sample
				assert y1.size()[0] == 1
				if y1[0,phen] == IGNORE_INDEX:
					#print "skip ",phen
					continue

				sys.stdout.write("\rPhen %d/%d, Sample %d/%d" % (phen, len(phenList), i+1, len(X)))
				if not saliencyG.has_key(corresp[i]):
					saliencyG[corresp[i]] = {}
				if not saliencyX.has_key(corresp[i]):
					saliencyX[corresp[i]] = {}

				sigma = self.noisePerc # inputs are standardized so std=1
				j = 0
				while j < self.n_iter:
					#print j
					x = x1.detach().clone()
					x = x + t.randn(x.size()) * sigma #gaussian noise with 0 mean and sigma std
					x = x.to(device)
					#print x.size()
					x.requires_grad_()
					parameters = list(self.model.parameters())+ [x]
					optimizer = t.optim.Adam(parameters)#+[{"params":[x], "lr":1}])
					optimizer.zero_grad()
					#y = y.to(device)
					yp = self.model.forward(x)
					assert yp.size()[0] == 1
					yp[0,phen].backward(retain_graph=True)
					#print tmpGradsG.shape, self.model.middle[1].weight.grad.cpu().size()
					tmpGradsG += np.array(self.model.middle[2].weight.grad.cpu())
					#print tmpGradsX.shape, x.grad[0,:,:].cpu().size()
					tmpGradsX += np.array(x.grad[0,:,:].cpu())
					optimizer.zero_grad()
					j+=1
				tmpGradsG = tmpGradsG / float(self.n_iter)
				tmpGradsX = tmpGradsX / float(self.n_iter)
				tmp = np.abs(np.sum(tmpGradsG,0))
				#print tmp.shape
				saliencyG[corresp[i]][phen] = sortGenes(tmp, genes, 100)
				#print tmpGradsX.shape, np.sum(tmpGradsX[:,:], 1).shape
				tmp = np.abs(np.sum(tmpGradsX[:,:], 1))
				#print tmp.shape
				saliencyX[corresp[i]][phen] = sortGenes(tmp, genes, 100)
				assert t.sum(self.model.middle[2].weight.grad) == 0
				assert t.sum(x.grad) == 0
				i += 1
		return saliencyG, saliencyX


class StandardSaliencyWrapper(NNwrapper, object):
	"""
	Class that implements the standard gradient based saliency maps.
	In the case of Galiana, stores gradients both at the "gene" neuron level and aggregates the features. Use the perSample method. Not sure what the other does.
	To save space, gives only the best 100 genes (instead of N_PHEN * 27k)
	"""

	def __init__(self, model):
		super(StandardSaliencyWrapper, self).__init__(model)
	
	def predictSALperSample(self, X, Y, corresp, device, saliency, genes, phenList, gradXinput = False, IGNORE_INDEX = -9999):
		#print len(phenList), Y.shape
		assert len(phenList) == Y.shape[1]
		saliencyG = saliency[0]
		saliencyX = saliency[1]
		self.model.eval()
		print "Predicting SAL..."
		dataset = ATDataset(X, Y)
		loader = DataLoader(dataset, batch_size=1, shuffle=False, sampler=None, num_workers=0)
		assert len(X) == len(Y) == len(corresp)
		i = 0
		for sample in loader:#runs only once
			x, y = sample
			sys.stdout.write("\rSample %d/%d" % (i+1, len(X)))
			x = x.to(device)
			x.requires_grad_()
			parameters = list(self.model.parameters())+ [x]
			optimizer = t.optim.Adam(parameters)#+[{"params":[x], "lr":1}])
			optimizer.zero_grad()
			y = y.to(device)
			yp = self.model.forward(x)
			#print yp.size()
			for phen, _ in enumerate(phenList):
				assert y.size()[0] == 1
				if y[0,phen] == IGNORE_INDEX:
					#print "skip ",phen
					continue
				#sys.stdout.write("\rphen %d/%d" % (phen, y.size()[1]))
				yp[0,phen].backward(retain_graph=True)
				if not saliencyG.has_key(corresp[i]):
					saliencyG[corresp[i]] = {}
				if not saliencyX.has_key(corresp[i]):
					saliencyX[corresp[i]] = {}
				#print self.model.inputTransform, self.model.middle
				#print self.model.middle[1].weight.grad.size()
				tmp = t.abs(t.sum(self.model.middle[2].weight.grad,0)).cpu()
				#raw
				#print "g grad"
				#print tmp, t.max(tmp)
				tmp = np.array(tmp)
				#tmp1 = tmp
				#r = self.sortGenes(tmp, genes, 100)
				#tmp = t.abs(x.grad[i,:,:])
				saliencyG[corresp[i]][phen] = sortGenes(tmp, genes, 100)

				if gradXinput:
					tmp = t.abs(t.sum(t.mul(x.grad, x)[0,:,:], 1)).cpu()
				else:
					tmp = t.abs(t.sum(x.grad[0,:,:], 1)).cpu()
				#print tmp.size()
				#print "x grad"
				#print tmp, t.max(tmp)
				#raw_input()
				#print tmp, tmp1
				saliencyX[corresp[i]][phen] = sortGenes(tmp, genes, 100)
				#saliency[corresp[i]].append(r)
				#saliency[corresp[i]].append(np.array(tmp.cpu(), dtype=np.float16))
				optimizer.zero_grad()
				assert t.sum(self.model.middle[2].weight.grad) == 0
				assert t.sum(x.grad) == 0
			i += 1
		return saliencyG, saliencyX

	def predictSALInput(self, X, Y, corresp, device, saliency, genes, IGNORE_INDEX = -9999):
		self.model.eval()
		print "Predicting SAL..."
		dataset = ATDataset(X, Y)
		loader = DataLoader(dataset, batch_size=len(X), shuffle=False, sampler=None, num_workers=0)
		assert len(X) == len(Y) == len(corresp)
		for sample in loader:#runs only once
			x, y = sample
			x = x.to(device)
			x.requires_grad_()
			y = y.to(device)
			yp = self.model.forward(x)
			#print yp.size()
			for phen in xrange(0, y.size()[1]):
				sys.stdout.write("\rphen %d/%d" % (phen, y.size()[1]))
				t.sum(yp[:,phen]).backward(retain_graph=True)
				i = 0
				while i < len(X):
					if not saliency.has_key(corresp[i]):
						saliency[corresp[i]] = []
					tmp = t.sum(t.abs(x.grad[i,:,:]), 1).cpu()
					r = sortGenes(tmp, genes, 100)
					#tmp = t.abs(x.grad[i,:,:])
					saliency[corresp[i]].append(r)
					#saliency[corresp[i]].append(np.array(tmp.cpu(), dtype=np.float16))
					i+=1
				x.grad.zero_()
				assert t.sum(x.grad) == 0
		return saliency

def sortGenes(grad, genes, THR):
	grad = grad.tolist()
	assert len(grad) == len(genes)
	geneNames = genes.keys()
	i = 0
	r = []
	while i < len(grad):
		r.append((grad[i], geneNames[i]))
		i+=1
	r = sorted(r, key=lambda x:x[0], reverse=True)[:THR]
	tmpGrad = []
	tmpGene = []
	for e in r:
		tmpGrad.append(e[0])
		tmpGene.append(e[1])
	return (tmpGene, np.array(tmpGrad))
	#print r[:THR]
	#return r[:THR]
