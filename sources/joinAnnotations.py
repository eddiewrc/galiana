#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readGenoList.py
#  
#  Copyright 2019 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import csv
import sources.parseMetaData as PMD

def readPhenoValues(f, db, genomes, phenotypes):
	"""
	This function takes each study CSV, and the genomes list and maps the phenotypes
	and their values to each genome ID, discarding the phenotypes without a matching WGS.

	Returns a {genomeId:{phenoNames:[list of values]}}
	"""
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	phenoNames = lines.pop(0).strip().split(",")
	#print phenoNames
	skipped = set()
	for l in lines:
		tmp = l.strip().split(",")
		#print tmp
		genomeId = tmp[0]
		if not genomeId in genomes:
			if genomeId not in skipped:
				#print "Skipping ", genomeId
				skipped.add(genomeId)
			continue
		if not genomeId in db:
			db[genomeId] = {}
		i = 2
		while i < len(tmp):
			if tmp[i] == '':
				i+=1
				continue
			if not phenoNames[i] in phenotypes:
				i+=1
				continue
			if not phenoNames[i] in db[genomeId]:
				db[genomeId][phenoNames[i]] = []
			db[genomeId][phenoNames[i]].append(float(tmp[i]))
			i+=1
	#print "Skipped (w/o corresp genome): %d" % len(skipped)
	return db

def integrateAnnotations(studiesDB, phenotypes, genomes, path, MIN_NUM_PHEN):
	"""
	This function integrates the phenotype data from the studies with the AT genomes
	by reading all the phenotypes annotated in each study and mapping them to the 
	corresponding AT genome id. 

	Returns {genomeID:{phenotypeName:[list of values]}}
	"""
	genomeAnnotation = {}
	for s in sorted(studiesDB.keys()):
		#print "Working on study ", s
		genomeAnnotation = readPhenoValues(path+s+"/study_"+s+"_values.csv", genomeAnnotation, genomes, phenotypes.keys())

	#checks that each phenotype has at least N values
	phenCount = {}
	removed = set()
	for g in genomeAnnotation.items():
		for p in g[1].keys():
			#print p
			if not p in phenCount:
				phenCount[p] = 0
			phenCount[p] += 1
	for p in phenCount.items():
		if p[1] < MIN_NUM_PHEN:
			removed.add(p[0])
			del phenotypes[p[0]]
	print ("Removed %d phenotypes because they have less than %d observed values" % (len(removed), MIN_NUM_PHEN))
	for g in genomeAnnotation.items():
		tmpDB = {}
		for p in g[1].keys():
			if p in removed:
				pass#del genomeAnnotation[g[0]][p]
			else:
				tmpDB[p] = genomeAnnotation[g[0]][p] #TODO:check this
		genomeAnnotation[g[0]] = tmpDB
	#end check
	noPhen = 0
	for g in genomeAnnotation.items():
		if not len(g[1]) > 0:
			del genomeAnnotation[g[0]]
			noPhen += 1
	print (" +++ Found %d genomes with no phenotype" % (noPhen))
	return genomeAnnotation, phenotypes # {genomeID: {phenotypes}}

def getRanges(db, PRINT = False):
	genomeAnnotation = db
	distroPhens = {}
	for g in genomeAnnotation.items():
		for val in g[1].items():
			p = val[0]
			if not p in distroPhens:
				distroPhens[p] = []
			distroPhens[p] += val[1]	
	ranges = {}
	for i in distroPhens.items():
		ranges[i[0]] = max(i[1]) - min(i[1])
		#print i[0], max(i[1]), min(i[1]), max(i[1]) - min(i[1])
	return ranges

def getPhenotypesRanges(phenotypesList, genomeAnnotation):
	ranges = getRanges(genomeAnnotation)
	tmp = []
	ofp = open("ranges.phenotypes.txt","w")
	ofp.write("Phenotype\tRange\tweightLoss\n")
	for f in phenotypesList:
		w = float(ranges[f])
		tmp.append(w)
		ofp.write(">%s\t%f\t%f\n" % (f, ranges[f], w))
	ofp.close()
	return tmp
	
def main(args):
	import numpy as np
	banned = ["Lifetime fitness in Germany and Spain under rainfall manipulation"]
	genomes = PMD.readGenoList("../metadata/genoList.csv")
	studies = PMD.readStudyList("../metadata/study_list.csv", banned)
	phenotypes = PMD.readPhenoList("../metadata/phenoList.csv", banned)
	genomeAnnotation, phenotypes = integrateAnnotations(studies, phenotypes, genomes, "../arapheno/studies/", 70)
	countPhenPerGen = []	
	countPhenType = {}
	distroPhens = {}
	for g in genomeAnnotation.items():
		#print g
		#raw_input()
		countPhenPerGen.append(len(g[1]))
		for val in g[1].items():
			p = val[0]
			if not p in countPhenType:
				countPhenType[p] = 0
			if not p in distroPhens:
				distroPhens[p] = []
			distroPhens[p] += val[1]	
			countPhenType[p] += 1
	print (sorted(countPhenPerGen))
	print (len(countPhenPerGen))
	#print sorted(countPhenType.items(), key=lambda x:x[1])
	#print genomeAnnotation["8214"]
	for i in distroPhens.items():
		#if len(i[1]) > 70:
		print ("%s\t%d\t%f\t%f\t%f\t%f\t%f" % (i[0], len(i[1]), np.mean(i[1]), np.std(i[1]), max(i[1]), min(i[1]), max(i[1]) - min(i[1])))
	#print distroPhens.items()[1]	
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
