#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readGenoList.py
#  
#  Copyright 2019 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import csv

def readGenoList(f):
	"""
	This function reads the CSV file listing all the AT genomes, their location, collector and number of phenotypes annotated on it.

	Returns {genomeID: {"name":name,"country":country, "collector":collector, "nPhen":phen}}

	"""

	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	lines.pop(0)
	db = {}
	for l in lines:
		tmp = l.strip().split(",")
		gid = tmp[0]
		name = tmp[1]
		country = tmp[2]
		try:
			gps = (float(tmp[3]), float(tmp[4]))
		except:
			gps = (None, None)
		collector = tmp[5]
		phen = int(tmp[10])
		db[gid] = {"name":name,"country":country, "collector":collector, "nPhen":phen, "gps":gps}
	print ("Found %d AT samples" % len(db))
	return db

def readPhenoList(f, bannedStudies = [], selectedPhen = [], bannedPhen = []):
	"""
	This function reads the CSV containing the list of available phenotypes and stores them into a data structure.

	bannedStudies contains the list (extended names) of the banned studies.

	Returns  {phenotypeName:{"pid":row["phenotype_id"], "study":row["study"], "desc":row["scoring"], "growth":row["growth_conditions"]}}

	"""
	csvReader = csv.DictReader(open(f))
	db = {}
	for row in csvReader:
		if row["name"] in db:
			#print "Duplicate: ", row["phenotype_id"]
			#raw_input()
			pass
		typePred = "regression"
		if "binary" in 	row["scoring"] or "yes" in row["scoring"]:
			typePred = "binary"
		if row["study"] in bannedStudies:
			continue
		if row["name"] in bannedPhen:
			continue
		if len(selectedPhen) > 0 and row["name"] not in selectedPhen:
			continue
		db[row["name"]] = {"pid":row["phenotype_id"], "type":typePred, "study":row["study"].replace("\t"," "), "desc":row["to_definition"].replace("\t"," "), "growth":row["growth_conditions"].replace("\t"," "), "scoring":row["scoring"].replace("\t"," "), "unit":row["uo_name"], "longName":row["to_name"].replace("\t"," ")}
		#print db[row["name"]]
	print( "Found %d phenotypes samples" % len(db))
	return db
	
def readStudyList(f, banned = []):	
	"""
	This function reads the CSV containing the info on the AT studies.

	Returns {studyID:{"name":row["name"], "desc":row["description"], "nPhen":row["phenotype_count"]}}
	"""
	csvReader = csv.DictReader(open(f))
	db = {}
	skip = 0
	for row in csvReader:
		if row["name"] in banned:
			skip += 1
			continue
		db[row["id"]] = {"name":row["name"], "desc":row["description"].replace("\t"," "), "nPhen":row["phenotype_count"]}
		#print db[row["name"]]
	assert skip == len(banned)
	print ("Found %d studies, banned %d/%d" % (len(db), skip, len(db)))
	return db
		

def main(args):
	readGenoList("../metadata/genoList.csv")
	readPhenoList("../metadata/phenoList.csv")
	readStudyList("../metadata/study_list.csv")
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
