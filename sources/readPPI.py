#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  senza nome.py
#  
#  Copyright 2019 eddiewrc <eddiewrc@alnilam>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import numpy as np
import torch as t

def readATgenes(f):
	"""
	This function reads all the AT genes used by annovar to annotate AT WGS. It is used also to create the numpy matrix contiaining the AT VCF data.

	Returns {ATgeneName:unique_number}
	"""
	ifp = open(f)
	line = ifp.readline()
	genes = {}
	c = 0
	while len(line) > 0:
		tmp = line.strip().split("\t")[12].split(":")[1]
		#print tmp
		if not genes.has_key(tmp):
			genes[tmp] = c
			c+=1
		line = ifp.readline()
	print "Found %d genes for AT" % len(genes)
	return genes

def readPPIs(f, genes):
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	adj = np.zeros((len(genes), len(genes)), dtype=np.int8)
	missing = 0
	for l in lines:
		tmp = l.strip().split("\t")
		try:
			adj[genes[tmp[0]]][genes[tmp[1]]] = 1
			adj[genes[tmp[1]]][genes[tmp[0]]] = 1
		except:
			missing += 1
	print "Missing: ", missing
	#print list(adj.sum(1))
	return adj


def readPPIsSparseTensor(f, genes):
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	coord = [[],[]]
	values = []	
	missing = 0

	for l in lines:
		tmp = l.strip().split("\t")
		try:
			x = genes[tmp[0]]
			y = genes[tmp[1]]
		except:
			missing += 1
			continue
		if x == y:
			continue
		coord[0].append(x)
		coord[1].append(y)
		values.append(1)
		coord[0].append(y)
		coord[1].append(x)
		values.append(1)
	print "Missing: ", missing
	assert len(coord[1]) == len(coord[0])
	assert len(coord[1]) == len(values)
	print "Number of edges: ", len(values)
	adj = t.sparse.FloatTensor(t.LongTensor(coord), t.FloatTensor(values), t.Size([len(genes),len(genes)]))
	return adj

def main(args):
	ATgenes = readATgenes("../atdb/AT_refGene.txt")
	#print ATgenes
	readPPIs("../PPI/DeBodt_predicted_PPI.txt", ATgenes)
	
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
