from scipy.stats import pearsonr
import numpy as np

def computeMultiRegrPerCountry(data, corresp, preds, labels, ignore_index, phenoList, phenotypes, ranges, fname):
	for c in data.nations.keys():
		tmpyp = []
		tmpy = []
		for i, s in enumerate(corresp):
			if not data.genomes[s]["country"] == c:
				continue
			tmpyp.append(preds[i])
			tmpy.append(labels[i])
		print ("Results within %s (%d samples)" % (c, len(tmpy)))
		if len(tmpy) < 2:
			print ("Skipping")
			continue
		computeScoresMultiRegr(np.array(tmpyp), np.array(tmpy), ignore_index, phenoList, phenotypes, ranges, fname+c+".txt")
	

def countryBasedSplitSklearn(genomes, genomesLookup):
	clusters = []
	countries = {}
	i = 0
	
	for g in genomes.items():
		co = g[1]["country"]
		if not countries.has_key(co):
			countries[co] = i
			i+=1
	assert max(countries.values())+1 == len(countries)
	ids = []
	for i, g in enumerate(genomesLookup.items()):
		clusters.append(countries[genomes[g[0]]["country"]])
		ids.append(g[0])
	return clusters, ids

def countryBasedSplit(genomes, NUM_SETS):
	NUM_SETS=5
	countries = {}
	for g in genomes.items():
		co = g[1]["country"]
		if not countries.has_key(co):
			countries[co] = set()
		countries[co].add(g[0])
	#for c in sorted(countries.items(), key=lambda x:len(x[1])):
	#	print "%s:%d" % (c[0], len(c[1]))
	sets = []
	i = 0
	while i < NUM_SETS:
		sets.append([])
		i+=1
	robin = 0
	go = True
	for l in sorted(countries.items(), key=lambda x:len(x[1]), reverse=True):		
		sets[robin] += list(l[1])
		#print robin
		if go:
			robin += 1
		else:
			robin -= 1
		if robin >= NUM_SETS:
			go = False	
			robin = 4
		elif robin <= 0:
			go = True
			robin = 0
	print ("CV sets: ")
	for l in sets:
		print (len(l))
	return sets

def computeScoresMultiRegr(preds, labels, ignore_index, phenoList, phenotypes, ranges, fname):
	numPhen = preds.shape[1]
	#print( preds.shape, labels.shape)
	assert preds.shape == labels.shape
	resMSE = []
	resMAE = []
	resPears = []
	pearsPval = []
	countPhen = []
	i = 0
	while i < numPhen:
		tmpPred = preds[:,i]
		tmpLabels = labels[:,i]
		countPhen.append(countPhenLabels(tmpLabels, ignore_index))
		#print(tmpPred, tmpLabels)
		if len(tmpPred) >= 7:
			p1, p2 = pearson(tmpPred, tmpLabels, ignore_index)
		else:
			p1 = None
		if p1 != None:
			resPears.append(p1)
			pearsPval.append(p2)
		else:
			resPears.append(-10)
			pearsPval.append(-10)
		resMSE.append(mse(tmpPred, tmpLabels, ignore_index))
		resMAE.append(mae(tmpPred, tmpLabels, ignore_index))
		i+=1
	i = 0
	assert len(resMSE) == len(resMAE) == len(phenoList)
	tmp = []
	while i < len(resMSE):
		tmp.append((phenoList[i], phenotypes[phenoList[i]]["pid"], resMAE[i], resPears[i], pearsPval[i], countPhen[i], phenotypes[phenoList[i]]["study"], phenotypes[phenoList[i]]["desc"]))
		i+=1
	tmp = sorted(tmp, key=lambda x:x[3], reverse=True)
	#tmp = sorted(tmp, key=lambda x:x[2]/x[3], reverse=False)
	if fname != None:
		ofp = open(fname+".phenoPreds","w")
		ofp.write( "PhenName\tpid\tMAE\tPearson\tpval\tnumSamples\tstudy\tdescription\n")
		i = 0
		assert len(resMSE) == len(resMAE) == len(phenoList)
		while i < len(resMSE):
			ofp.write("%s\t%s\t%.3f\t%.3f\t%e\t%d\t%s\t%s\n" % tmp[i])
			i+=1
	mseScore = sum(resMSE)/float(len(resMSE))
	maeScore = sum(resMAE)/float(len(resMAE))
	resPears = np.array(resPears)
	resPears = resPears[resPears != -10]
	if len(resPears) > 0:
		pearsScore = sum(resPears)/float(len(resPears))
	else:
		pearsScore = -9999
	if np.isnan(pearsScore):
		print( resPears)
		raw
	print( "--------------------------------------")
	print( "Mean MSE: %.3f " % mseScore)
	print( "Mean MAE: %.3f " % (maeScore))
	print( "Mean Pearson: %.3f " % (pearsScore))
	print( "--------------------------------------")
	return mseScore, maeScore, pearsScore

def countPhenLabels(labels, ignore_index):
	c = 0
	for i in labels:
		if i != ignore_index:
			c+=1
	return c

def pearson(s1, s2, ignore_index):
	r1 = []
	r2 = []
	cases = 0
	i = 0
	assert len(s1) == len (s2)
	while i < len(s1):
		if s2[i] == ignore_index:
			i+=1
			continue
		r1.append(s1[i])
		r2.append(s2[i])
		i+=1
	if len(r2) <= 1:
		return None, None
	else:
		tmp = pearsonr(r1,r2)
		if len(r1) <= 2 or np.isnan(tmp[0]):
			return None, None
		return tmp

def mae(s1, s2, ignore_index):
	r = 0
	i = 0
	assert len(s1) == len (s2)
	while i < len(s1):
		if s2[i] == ignore_index:
			i+=1
			continue
		r += abs(s1[i] - s2[i])
		i+=1
	return r / float(len(s1))

def mse(s1, s2, ignore_index):
	r = 0
	i = 0
	assert len(s1) == len (s2)
	while i < len(s1):
		if s2[i] == ignore_index:
			i+=1
			continue
		r += (s1[i] - s2[i])**2
		i+=1
	return r / float(len(s1))

def checkMinMax(Y, m1=0, M1=1):
	print (Y.shape)
	warn = set()
	for i in range(0, Y.shape[1]):
		m = min(Y[:,i])
		M = max(Y[:,i])
		if m <= m1 or M >= M1:
			warn.add(i)
	if len(warn) == 0:
		print (" ### MinMax OK!")
	else:
		print (" ### Following labels are not MinMax ed: ", warn)

def checkStandardiz(Y, ignore_index):
	warn = set()
	for i in range(0, Y.shape[1]):
		tmp = Y[:,i]
		tmp = tmp[tmp != ignore_index]
		m = np.mean(tmp)
		std = np.var(tmp)
		if abs(m) > 0.01:
			warn.add(i)
		if abs(std-1)> 0.05:
			warn.add(i)
	if len(warn) == 0:
		print( " ### Standardization OK!")
	else:
		print (" ### Following labels are not standardized: ", warn)


class StructuredScaler:
	def __init__(self, scaler):
		self.scaler = scaler
		
	def fit_transform(self, X):
		if type(X) != np.array:
			X = np.array(X)
		shape = X.shape
		print (shape)
		X = X.reshape(shape[0]*shape[1], shape[2])
		X = self.scaler.fit_transform(X)
		return X.reshape(shape[0], shape[1], shape[2])
		
	def transform(self, X):
		if type(X) != np.array:
			X = np.array(X)
		shape = X.shape
		X = X.reshape(shape[0]*shape[1], shape[2])
		X = self.scaler.transform(X)
		return X.reshape(shape[0], shape[1], shape[2])

	
class PerGeneScaler:
	def __init__(self, scaler):
		self.scaler = scaler
		
	def fit_transform(self, X):
		if type(X) != np.array:
			X = np.array(X)
		shape = X.shape
		print( shape)
		X = X.reshape(shape[0], shape[1]* shape[2])
		X = self.scaler.fit_transform(X)
		return X.reshape(shape[0], shape[1], shape[2])
		
	def transform(self, X):
		if type(X) != np.array:
			X = np.array(X)
		shape = X.shape
		X = X.reshape(shape[0], shape[1]* shape[2])
		X = self.scaler.transform(X)
		return X.reshape(shape[0], shape[1], shape[2])

	
